#ifndef ETA_HR_MONITOR
#define ETA_HR_MONITOR

#include <stdexcept>
#include <vector>
#include <string>
#include <deque>

enum {
	HIGH_RATE = 1,
	ABNORMAL_INCREASE = 2,
	ABNORMAL_DECREASE = 3,
	ABNORMAL_FLUCT = 4,
	LOW_RATE = 5,

	// Now healthy states:
	INCREASE_OK = 6,
	DECREASE_OK = 7,
	STABLE = 8
};

class hr_monitor {
public:
	static constexpr bool is_healthy(int state) {
		return !(state == HIGH_RATE || 
			state == ABNORMAL_FLUCT || 
			state == ABNORMAL_DECREASE || 
			state == ABNORMAL_INCREASE ||
			state == LOW_RATE);
	}

	static std::string state_string(int state) {
		switch (state) {
		case HIGH_RATE:
			return "Abnormally high heart rate";
		case ABNORMAL_INCREASE:
			return "Abnormal increase in heart rate";
		case ABNORMAL_DECREASE:
			return "Abnormal decrease in heart rate";
		case ABNORMAL_FLUCT:
			return "Abnormal fluctuation in heart rate";
		case INCREASE_OK:
			return "Healthy increase in heart rate";
		case DECREASE_OK:
			return "Healthy decrease in heart rate";
		case LOW_RATE:
			return "Abnormally low heart rate";
		case STABLE:
			return "Heart rate remained stable";
		}

		throw std::invalid_argument("Invalid state argument passed to state_string");
	}

	hr_monitor(int lim, int age) :_hr_lim(lim), _age(age) {
		if (lim <= 0)
			throw std::invalid_argument("Number of recorded heart rates must be at least 1");
		if (age <= 0)
			throw std::invalid_argument("Age has to be positive, dumbass...");
	}

	hr_monitor(int age) :hr_monitor(_DEFAULT_LIM, age) { }
	void insert(int);
	std::deque<int> heart_queue();
	int check_state();
	void clear();
	int age() { 
		return _age; 
	}

private:
	static constexpr int _DEFAULT_LIM = 15; // 15 readings at a time.
	std::vector<int> _diffs;
	std::deque<int> _heart_rates;
	std::vector<int> _trends;
	int _hr_lim;
	int _age;
	void _get_trends();
};

#endif