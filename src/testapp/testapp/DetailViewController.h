//
//  DetailViewController.h
//  testapp
//
//  Created by Wassim Omais on 1/26/16.
//  Copyright © 2016 Wassim Omais. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

